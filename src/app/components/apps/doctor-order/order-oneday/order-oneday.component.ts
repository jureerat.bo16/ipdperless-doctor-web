import { Component, OnInit } from '@angular/core';
import { DoctorOrderService } from '../doctor-order-service';
import { LookupService } from '../../../../shared/lookup-service';
import { ActivatedRoute, Router } from '@angular/router';
import { DateTime } from 'luxon';
import * as _ from 'lodash';
interface AutoCompleteCompleteEvent {
    originalEvent: Event;
    query: string;
}

interface Option {
    label: string;
    name: string;
    id: string;
    code: string;
    default_usage: string;
}
interface OptionItems {
    id: string;
    name: string;
    default_usage: string;
    item_type_id: string;
}
interface OptionDrugsUsage {
    id: string;
    usage_code: string;
    name: string;
}
@Component({
    selector: 'app-order-oneday',
    templateUrl: './order-oneday.component.html',
    styleUrls: ['./order-oneday.component.scss'],
})
export class OrderOnedayComponent implements OnInit {
    sidebarVisible2: boolean = false;
    queryParamsData: any;
    dxData: any;
    pageSize = 20;
    pageIndex = 1;
    offset: any;
    total: any;
    dataSet: any;
    showDx: boolean = true;
    dxName: any;

    orderContinueV: any;
    orderOnedayV: any;

    orderContinueValue: { label: string }[] = [];
    // orderOndayValue: { items: any,usage:any,quantity:any }[] = [];
    orderOndayValue: any = [];

    selectedCountry: any;
    filteredCountries: any | undefined;

    selectedItems: any;
    filteredItems: any | undefined;

    selectedUsage: any;
    filteredUsage: any | undefined;

    selectedQuantity: any;
    filteredQuantity: any | undefined;

    subjective: any;
    objective: any;
    assertment: any;
    plan: any;
    note: any;

    progress_note: any;
    progress_note_subjective: any[] = [];
    progress_note_objective: any[] = [];
    progress_note_assertment: any[] = [];
    progress_note_plan: any[] = [];

    patientList: any;
    orderList: any;
    drugList: any;

    orders: any[] = [];
    optionsDrugs: OptionItems[] = [];
    optionsAllItems: OptionItems[] = [];

    countries: any = [
        { name: 'Afghanistan', code: 'AF' },
        { name: 'Åland Islands', code: 'AX' },
        { name: 'Albania', code: 'AL' },
        { name: 'Algeria', code: 'DZ' },
        { name: 'American Samoa', code: 'AS' },
        { name: 'Andorra', code: 'AD' },
        { name: 'Angola', code: 'AO' },
        { name: 'Anguilla', code: 'AI' },
        { name: 'Antarctica', code: 'AQ' },
        { name: 'Antigua and Barbuda', code: 'AG' },
    ];
    optionsDrugsUsage: OptionDrugsUsage[] = [
        { usage_code: '1prn', name: '1 เม็ต เวลาปวด', id: '1' },
        { usage_code: '2tp', name: '2 แค็ป เช้าเย็น', id: '2' },
        { usage_code: '3pan', name: '3 เม็ด หลังอาหารเช้าเย็น', id: '3' },
        { usage_code: '3tba', name: '3 เม็ด หลังอาหารทันที', id: '4' },
        { usage_code: '1dap', name: '1 เม็ดตอนเช้าทุกวัน', id: '5' },
        { usage_code: 'im', name: 'สำหรับฉีกเข้ากล้ามเนื้อ', id: '6' },
    ];

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private lookupService: LookupService,
        private doctorOrderService: DoctorOrderService
    ) {
        let jsonString: any =
            this.activatedRoute.snapshot.queryParamMap.get('data');
        const jsonObject = JSON.parse(jsonString);
        this.queryParamsData = jsonObject;
    }

    ngOnInit(): void {
        console.log('progressNote');
        this.getDoctorOrderById();
        this.getLookupMedicine();
    }
    filterItems(event: AutoCompleteCompleteEvent) {
        let filtered: any[] = [];
        let query = event.query.toLowerCase();

        for (let i = 0; i < this.optionsAllItems.length; i++) {
            let item = this.optionsAllItems[i];
            if (item.name.toLowerCase().indexOf(query) !== -1) {
                filtered.push(item);
            }
        }
        this.filteredItems = filtered;
    }
    filterUsage(event: AutoCompleteCompleteEvent) {
        let filtered: any[] = [];
        let query = event.query.toLowerCase();

        for (let i = 0; i < this.optionsDrugsUsage.length; i++) {
            let item = this.optionsDrugsUsage[i];
            if (item.name.toLowerCase().indexOf(query) !== -1 || item.usage_code.toLowerCase().indexOf(query) !== -1) {
                filtered.push(item);
            }
        }
        this.filteredUsage = filtered;
    }

    selectedItemsClear() {
        this.selectedItems = '';
    }
    selectedUsageClear() {
        this.selectedUsage = '';
    }
    selectedQuantityClear() {
        this.selectedQuantity = '';
    }

    saveOrderOneday() {
        // this.orderOndayValue.push({ items: this.selectedItems,usage:this.selectedUsage,quantity:this.selectedQuantity });
        this.orderOndayValue.push({
            items: this.selectedItems,
            usage: this.selectedUsage,
            quantity: this.selectedQuantity,
        });
        console.log(this.orderOndayValue);
        this.selectedItems = '';
    }

    filterCountry(event: AutoCompleteCompleteEvent) {
        let filtered: any[] = [];
        let query = event.query;

        for (let i = 0; i < (this.countries as any[]).length; i++) {
            let country = (this.countries as any[])[i];
            if (country.name.toLowerCase().indexOf(query.toLowerCase()) == 0) {
                filtered.push(country);
            }
        }

        this.filteredCountries = filtered;
    }

    getProgressNote() {
        console.log('progressNote');
        this.sidebarVisible2 = true;

        this.getList();
        //this.getProgresnote();
        this.getDx();
    }

    async getDx() {
        try {
            let response = await this.lookupService.getDx();
            let _dxData = response.data.data;
            this.dxData = _.sortBy(_dxData, [
                function (o) {
                    return o.name;
                },
            ]);
            console.log(this.dxData);
        } catch (error: any) {
            console.log('getLabItems' + `${error.code} - ${error.message}`);
        }
    }
    async getList() {
        try {
            const _limit = this.pageSize;
            const _offset = this.offset;
            const response = await this.doctorOrderService.getWaiting(
                _limit,
                _offset
            );

            const data: any = response.data;
            console.log(data);

            this.total = data.total || 1;

            this.dataSet = data.data.map((v: any) => {
                const date = v.admit_date
                    ? DateTime.fromISO(v.admit_date)
                          .setLocale('th')
                          .toLocaleString(DateTime.DATETIME_SHORT_WITH_SECONDS)
                    : '';
                v.admit_date = date;
                return v;
            });
            console.log(this.dataSet);
        } catch (error: any) {
            console.log(`${error.code} - ${error.message}`);
        }
    }

    addOrderContinue(): void {
        this.orderContinueValue.push({ label: this.orderContinueV });
        this.orderContinueV = '';
    }

    removeOrderContinue(label: string) {
        this.orderContinueValue = this.orderContinueValue.filter(
            (item) => item.label !== label
        );
        console.log(this.orderContinueValue);
    }

    async getDoctorOrderById() {
        try {
            const _limit = this.pageSize;
            const _offset = this.offset;
             console.log(this.queryParamsData);

            const response = await this.doctorOrderService.getDoctorOrderById(
                this.queryParamsData
            );

             console.log(response);

            const data: any = response.data;
            this.progress_note = {};

            this.orderList = await data.data[0];
            console.log(this.orderList);
            /*
          this.subjective = this.orderList.progress_note.subjective;
          this.objective = this.orderList.progress_note.objective;
          this.assertment = this.orderList.progress_note.assertment;
          this.plan = this.orderList.progress_note.plan;
    
          */
            this.subjective = this.progress_note.subjective;
            this.objective = this.progress_note.objective;
            this.assertment = this.progress_note.assertment;
            this.plan = this.progress_note.plan;
            console.log(this.orderList);
            let _order: any = [];
            for (let d of data.data) {
                _order.push(d.order);
            }
            this.orders = _order;
            // this.medicine_usage_extra=this.orderList.order.medicine_usage_extra;
            // this.quantity=this.orderList.order.quantity;
            //console.log(_order);

            // this.an = this.patientList.an;
            // this.title = this.patientList.patient.title;
            // this.fname = this.patientList.patient.fname;
            // this.lname = this.patientList.patient.lname;
            // this.gender = this.patientList.patient.gender;
            // this.age = this.patientList.patient.age;
            // this.address = this.patientList.address;
            // this.phone = this.patientList.phone;

            // this.total = data.total || 1
            /*  console.log(data);
          this.dataSet =data.data.map((v: any) => {
            const date = v.admit_date
              ? DateTime.fromISO(v.admit_date)
                  .setLocale('th')
                  .toLocaleString(DateTime.DATETIME_SHORT_WITH_SECONDS)
              : '';
            v.admit_date = date;
            return v;
          });*/
        } catch (error: any) {
            console.log(error);
        }
    }
    async getLookupMedicine() {
        try {
            let response = await this.doctorOrderService.getLookupMedicine();
            this.optionsDrugs = response.data.data; 

            // console.log(this.optionsDrugs);

            this.optionsDrugs.forEach((item) => {
                this.optionsAllItems.push(item);
            });
            this.getLabItems();
        } catch (error: any) {
            console.log('getLabItems' + `${error.code} - ${error.message}`);
        }
    }
    async getLookupMedicineUsage() {
        try {
            let response = await this.doctorOrderService.getLookupMedicine();
            this.optionsDrugs = response.data.data;

            // console.log(this.optionsDrugs);
        } catch (error: any) {
            console.log(error);
        }
    }
    optionsLabs: OptionItems[] = [];

    async getLabItems() {
        const itemTypeID = 1;
        console.log('getLabItems');

        try {
            let response = await this.doctorOrderService.getItems(itemTypeID);
            // console.log(response);
            this.optionsLabs = response.data.data;

            // console.log(this.optionsLabs);

            this.optionsLabs.forEach((item) => {
                this.optionsAllItems.push(item);
            });

            this.getXrayItems();
        } catch (error: any) {
            console.log(`${error.code} - ${error.message}`);
        }
    }

    optionsXray: OptionItems[] = [];

    async getXrayItems() {
        const itemTypeID = 2;
        console.log('getXrayItems');

        try {
            let response = await this.doctorOrderService.getItems(itemTypeID);
            // console.log(response);
            this.optionsXray = response.data.data;

            // console.log(this.optionsXray);

            this.optionsXray.forEach((item) => {
                this.optionsAllItems.push(item);
            });
            //  console.log(this.optionsAllItems);
            this.getProcedureItems();
        } catch (error: any) {
            console.log('getXrayItems=' + `${error.code} - ${error.message}`);
        }
    }

    optionsProcedure: OptionItems[] = [];

    async getProcedureItems() {
        const itemTypeID = 3;
        console.log('getProcedureItems');
        try {
            let response = await this.doctorOrderService.getItems(itemTypeID);
            // console.log(response);
            this.optionsProcedure = response.data.data;

            // console.log(this.optionsProcedure);

            this.optionsProcedure.forEach((item) => {
                this.optionsAllItems.push(item);
            });
            // console.log(this.optionsAllItems);
        } catch (error: any) {
            console.log(
                'getProcedureItems=' + `${error.code} - ${error.message}`
            );
        }
    }
}
