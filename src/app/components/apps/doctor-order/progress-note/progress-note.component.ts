import { Component, OnInit } from '@angular/core';
import { DoctorOrderService } from '../doctor-order-service';
import { LookupService } from '../../../../shared/lookup-service';
import { ActivatedRoute, Router } from '@angular/router';
import { DateTime } from 'luxon';
import * as _ from 'lodash';

@Component({
    selector: 'app-progress-note',
    templateUrl: './progress-note.component.html',
    styleUrls: ['./progress-note.component.scss'],
})
export class ProgressNoteComponent implements OnInit {
    sidebarVisible2: boolean = false;
    queryParamsData: any;
    dxData: any;
    pageSize = 20;
    pageIndex = 1;
    offset: any;
    total: any;
    dataSet: any;
    showDx: boolean = true;
    dxName: any;
    optionSubjective: any;
    optionsAssessment: any;
    optionPlan: any;
    showProgressNote: boolean = false;
    optionSubjectiveSelected: any;
    optionObjective: any = [];
    optionObjectiveSelected: any;
    optionAssessmentSelected: any;
    optionPlanSelected: any;
    progressValueS: any;
    progressValueO?: any;
    progressValueA?: any;
    progressValueP?: any;
    progressValueN?: any;

    progress_note_subjective: any[] = [];
    progress_note_objective: any[] = [];
    progress_note_assertment: any[] = [];
    progress_note_plan: any[] = [];
    progress_note: any[] = [];
    // progressValue: any = [];
    progressValue: { label: string }[] = [];
    dxLoading: boolean = false;

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private lookupService: LookupService,
        private doctorOrderService: DoctorOrderService
    ) {
        let jsonString: any =
            this.activatedRoute.snapshot.queryParamMap.get('data');
        const jsonObject = JSON.parse(jsonString);
        this.queryParamsData = jsonObject;
    }
    ngOnInit(): void {
        console.log('progressNote');
    }
    getProgressNote() {
        console.log('progressNote');
        this.sidebarVisible2 = true;

        this.getList();
        //this.getProgresnote();
        this.getDx();
    }

    async getDx() {
        this.dxLoading = true;
        try {
            let response = await this.lookupService.getDx();
            let _dxData = response.data.data;
            this.dxData = _.sortBy(_dxData, [
                function (o) {
                    return o.name;
                },
            ]);
            console.log(this.dxData);
            this.dxLoading = false;
        } catch (error: any) {
            console.log('getDx' + `${error.code} - ${error.message}`);
            this.dxLoading = false;
        }
    }
    async getList() {
        try {
            const _limit = this.pageSize;
            const _offset = this.offset;
            const response = await this.doctorOrderService.getWaiting(
                _limit,
                _offset
            );

            const data: any = response.data;
            console.log(data);

            this.total = data.total || 1;

            this.dataSet = data.data.map((v: any) => {
                const date = v.admit_date
                    ? DateTime.fromISO(v.admit_date)
                        .setLocale('th')
                        .toLocaleString(DateTime.DATETIME_SHORT_WITH_SECONDS)
                    : '';
                v.admit_date = date;
                return v;
            });
            console.log(this.dataSet);
        } catch (error: any) {
            console.log(`${error.code} - ${error.message}`);
        }
    }

    async dxlistClick(id: any, name: any) {
        console.log(id);

        try {
            let response = await this.lookupService.getStanding(id);
            //  let progessnoteData: any = response.data.data;
            // this.optionsDrugsUsage = response.data.data;
            console.log(response);
            this.dxName = name;
            let progessnoteData = response.data.data;
            this.optionSubjective = progessnoteData.filter(
                (item: any) => item.progress_note_type_code == 'S'
            );
            this.optionSubjective = progessnoteData.filter(
                (item: any) => item.progress_note_type_code == 'O'
            );
            this.optionsAssessment = progessnoteData.filter(
                (item: any) => item.progress_note_type_code == 'A'
            );
            this.optionPlan = progessnoteData.filter(
                (item: any) => item.progress_note_type_code == 'P'
            );

            this.showProgressNote = true;
            this.showDx = false;

            // this.optionSubjective = progessnoteData.filter((item: any) => item.progress_note_type_code == 'S')
            // this.optionObjective = progessnoteData.filter((item: any) => item.progress_note_type_code == 'O')
            // this.optionsAssessment = progessnoteData.filter((item: any) => item.progress_note_type_code == 'A')
            // this.optionPlan = progessnoteData.filter((item: any) => item.progress_note_type_code == 'P')
        } catch (error: any) {
            console.log('getLabItems' + `${error.code} - ${error.message}`);
        }
    }
    optionSubjectClick(e: any) {
        if (e.target.checked) {
            this.optionSubjectiveSelected.push(e.target.value);
        } else {
            this.optionSubjectiveSelected.splice(
                this.optionSubjectiveSelected.indexOf(e.target.value),
                1
            );
            // this.optionSubjectiveSelected.pop(e.target.value);
        }

        console.log(this.optionSubjectiveSelected);
    }
    optionObjectClick(e: any) {
        if (e.target.checked) {
            this.optionObjectiveSelected.push(e.target.value);
        } else {
            this.optionObjectiveSelected.splice(
                this.optionObjectiveSelected.indexOf(e.target.value),
                1
            );
            // this.optionSubjectiveSelected.pop(e.target.value);
        }

        console.log(this.optionObjectiveSelected);
    }
    optionAssessmentClick(e: any) {
        if (e.target.checked) {
            this.optionAssessmentSelected.push(e.target.value);
        } else {
            this.optionAssessmentSelected.splice(
                this.optionAssessmentSelected.indexOf(e.target.value),
                1
            );
            // this.optionSubjectiveSelected.pop(e.target.value);
        }

        console.log(this.optionAssessmentSelected);
    }

    optionPlanClick(e: any) {
        if (e.target.checked) {
            this.optionPlanSelected.push(e.target.value);
        } else {
            this.optionPlanSelected.splice(
                this.optionPlanSelected.indexOf(e.target.value),
                1
            );
            // this.optionSubjectiveSelected.pop(e.target.value);
        }

        console.log(this.optionPlanSelected);
    }
    async saveProgressNote() {
        const format1 = 'YYYY-MM-DD HH:mm:ss';
        const format2 = 'YYYY-MM-DD';
        const format3 = 'HH:mm:ss';
        var date1 = new Date('2020-06-24 22:57:36');
        var date2 = new Date();
        var date3 = new Date();
        // var test =sub.id;
        //  console.log("saveProgressNote="+test);
        let data = {
            doctor_order: {
                admit_id: this.queryParamsData,
                doctor_order_date: '',
                doctor_order_time: '',
                doctor_order_by: this.queryParamsData,
                is_confirm: true,
            },
        };
        try {
            this.doctorOrderService.saveProgressNote(data);
        } catch (error) {
            console.log('saveProgressNote=' + error);
        }
    }

    addProgressNoteS(): void {
        this.progress_note_subjective.push({ label: this.progressValueS });

        let progress_note = {
            subjective: [this.progressValueS],
        };
        this.progressValueS = '';
        this.saveDoctorOrder(progress_note);
    }

    addProgressNoteO(): void {
        this.progress_note_objective.push({ label: this.progressValueO });
        let progress_note = {
            objective: [this.progressValueO],
        };
        this.progressValueO = '';
        this.saveDoctorOrder(progress_note);
    }

    addProgressNoteA(): void {
        this.progress_note_assertment.push({ label: this.progressValueA });
        let progress_note = {
            assertment: [this.progressValueA],
        };
        this.progressValueA = '';
        this.saveDoctorOrder(progress_note);
    }

    addProgressNoteP(): void {
        this.progress_note_plan.push({ label: this.progressValueP });
        let progress_note = {          
            plan: [this.progressValueP],
       
        };
        this.progressValueP = '';
        this.saveDoctorOrder(progress_note);
    }
    addProgressNote(): void {
        this.progress_note.push({ label: this.progressValueN });
        let progress_note = {           
            note: this.progressValueN,
        };
        this.progressValueN = '';
        this.saveDoctorOrder(progress_note);
    }

    removeProgressNote(label: string) {
        this.progressValue = this.progressValue.filter(
            (item) => item.label !== label
        );
        console.log(this.progressValue);
    }

    async saveDoctorOrder(datas: any) {
        // luxon
        var date2 = DateTime.now();
        var date3 = DateTime.now();
        const format2 = 'yyyy-MM-dd';
        const format3 = 'HH:mm:ss';
        const date5 = date2.toFormat('yyyy-MM-dd');
        const time1 = date3.toFormat('HH:mm:ss');

        let data = {
            doctor_order: {
                admit_id: this.queryParamsData,
                doctor_order_date: date5,
                doctor_order_time: time1,
                doctor_order_by: this.queryParamsData,
                is_confirm: true,
            },
            progress_note: datas,
        };

        try {
            console.log(data);

            const response = await this.doctorOrderService.saveDoctorOrder(
                data
            );
            console.log(response);
        } catch (error) {
            console.log('saveDoctorOrder=' + error);
        }
    }
}
