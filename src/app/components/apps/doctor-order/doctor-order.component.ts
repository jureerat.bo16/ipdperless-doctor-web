import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Message, MessageService } from 'primeng/api';

import { DoctorOrderService } from './doctor-order-service';


@Component({
    selector: 'app-doctor-order',
    templateUrl: './doctor-order.component.html',
    styleUrls: ['./doctor-order.component.scss'],
    providers: [MessageService],
    
})
export class DoctorOrderComponent implements OnInit {
    query: any = '';
    dataSet: any[] = [];
    loading = false;

    total = 0;
    pageSize = 20;
    pageIndex = 1;
    offset = 0;
    user_login_name: any;

    progressValueS?: string;
    progressValueO?: string;
    progressValueA?: string;
    progressValueP?: string;
    onedayValue?: string;
    continueValue?: string;

    progressTags = ['progressValue'];
    onedayTags = ['onedayValue'];
    continueTags = ['onedayValue'];

    inputVisible = false;
    inputValue?: string = '';

    patientList: any;
    orderList: any;
    drugList: any;

    hn: any;
    an: any;

    title: any;
    fname: any;
    lname: any;
    gender: any;
    age: any;
    address: any;
    phone: any;

    admit_id: any;
    doctor_order_date: any;
    doctor_order_time: any;
    doctor_order_by: any;
    is_confirm: any;

    subjective: any;
    objective: any;
    assertment: any;
    plan: any;
    note: any;

    order_type_id: any;
    item_type_id: any;
    item_id: any;
    item_name: any;
    medicine_usage_code: any;
    medicine_usage_extra: any;
    quantity: any;

    food_id: any;
    start_date: any;
    start_time: any;
    end_date: any;
    end_time: any;

    is_vital_sign: any;

    progress_note: any;
    progress_note_subjective: any[] = [];
    progress_note_objective: any[] = [];
    progress_note_assertment: any[] = [];
    progress_note_plan: any[] = [];
    oneDay: any[] = [];
    continue: any[] = [];
    orders: any[] = [];
    ordersOneday: any[] = [];
    continues: any[] = [];

    medicine: any[] = [];

    progress_note_id: any;

    drugname: any;
    indeterminate = true;

    queryParamsData: any;
    value?: string;
    inputValue1?: string;
    jsonData:any={
        
    }

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private spinner: NgxSpinnerService,
        private messageService: MessageService,

        private doctorOrderService: DoctorOrderService
    ) {
        let jsonString: any =
            this.activatedRoute.snapshot.queryParamMap.get('data');
        const jsonObject = JSON.parse(jsonString);
        this.queryParamsData = jsonObject;
    }

    ngOnInit(): void {
        console.log('me');
        this.getPatient();
    }


    async getPatient() {
        try {
            const _limit = this.pageSize;
            const _offset = this.offset;
            //  console.log(this.queryParamsData);

            const response = await this.doctorOrderService.getPatientInfo(
                this.queryParamsData
            );

            const data: any = response.data;

            this.patientList = await data.data;
            //console.log(this.patientList);
            this.hn = this.patientList.hn;
            this.an = this.patientList.an;
            this.title = this.patientList.patient.title;
            this.fname = this.patientList.patient.fname;
            this.lname = this.patientList.patient.lname;
            this.gender = this.patientList.patient.gender;
            this.age = this.patientList.patient.age;
            this.address = this.patientList.address;
            this.phone = this.patientList.phone;

            this.total = data.total || 1;
            this.messageService.add({
                severity: 'success',
                summary: 'กระบวนการสำเร็จ #',
                detail: '.....',
            });
        } catch (error: any) {
            console.log(error);
            // this.message.error('getPatient()' +`${error.code} - ${error.message}`);
            this.messageService.add({
                severity: 'dager',
                summary: 'เกิดข้อผิดพลาด #',
                detail: 'กรุณาติดต่อผู้ดูแลระบบ...' + error,
            });
        }
    }
}
