import { Component, OnInit } from '@angular/core';
import { DoctorOrderService } from '../doctor-order-service';
import { LookupService } from '../../../../shared/lookup-service';
import { ActivatedRoute, Router } from '@angular/router';
import { DateTime } from 'luxon';
import * as _ from 'lodash';

@Component({
    selector: 'app-order-continue',
    templateUrl: './order-continue.component.html',
    styleUrls: ['./order-continue.component.scss'],
})
export class OrderContinueComponent implements OnInit {
    sidebarVisible2: boolean = false;
    queryParamsData: any;
    dxData: any;
    pageSize = 20;
    pageIndex = 1;
    offset: any;
    total: any;
    dataSet: any;
    showDx: boolean = true;
    dxName: any;

    orderContinueV: any;
   
    orderContinueValue: { label: string }[] = [];

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private lookupService: LookupService,
        private doctorOrderService: DoctorOrderService
    ) {
        let jsonString: any =
            this.activatedRoute.snapshot.queryParamMap.get('data');
        const jsonObject = JSON.parse(jsonString);
        this.queryParamsData = jsonObject;
    }
    ngOnInit(): void {
        console.log('progressNote');
    }
    getProgressNote() {
        console.log('progressNote');
        this.sidebarVisible2 = true;

        this.getList();
        //this.getProgresnote();
        this.getDx();
    }

    async getDx() {
        try {
            let response = await this.lookupService.getDx();
            let _dxData = response.data.data;
            this.dxData = _.sortBy(_dxData, [
                function (o) {
                    return o.name;
                },
            ]);
            console.log(this.dxData);
        } catch (error: any) {
            console.log('getLabItems' + `${error.code} - ${error.message}`);
        }
    }
    async getList() {
        try {
            const _limit = this.pageSize;
            const _offset = this.offset;
            const response = await this.doctorOrderService.getWaiting(
                _limit,
                _offset
            );

            const data: any = response.data;
            console.log(data);

            this.total = data.total || 1;

            this.dataSet = data.data.map((v: any) => {
                const date = v.admit_date
                    ? DateTime.fromISO(v.admit_date)
                          .setLocale('th')
                          .toLocaleString(DateTime.DATETIME_SHORT_WITH_SECONDS)
                    : '';
                v.admit_date = date;
                return v;
            });
            console.log(this.dataSet);
        } catch (error: any) {
            console.log(`${error.code} - ${error.message}`);
        }
    }

   

    addOrderContinue(): void {
        this.orderContinueValue.push({ label: this.orderContinueV });
        this.orderContinueV = '';
    }

   
    removeOrderContinue(label: string) {
        this.orderContinueValue = this.orderContinueValue.filter(
            (item) => item.label !== label
        );
        console.log(this.orderContinueValue);
    }
}
