import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule  } from '@angular/forms';

import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { DataViewModule } from 'primeng/dataview';
import { DropdownModule } from 'primeng/dropdown';
import { ButtonModule } from 'primeng/button';
import { DividerModule } from 'primeng/divider';
import { AvatarModule } from 'primeng/avatar';
import { NgxSpinnerModule } from "ngx-spinner";

import { DragDropModule } from '@angular/cdk/drag-drop';
import { ListPatientRoutingModule } from './list-patient-routing.module';
import { ListPatientComponent } from './list-patient.component';


@NgModule({
  declarations: [
    ListPatientComponent
  ],
  imports: [
    CommonModule,FormsModule,ReactiveFormsModule ,NgxSpinnerModule,
    ListPatientRoutingModule,DragDropModule,DataViewModule,DropdownModule,ButtonModule,DividerModule,AvatarModule
  ]
})
export class ListPatientModule { }
